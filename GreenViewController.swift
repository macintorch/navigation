//
//  GreenViewController.swift
//  RedGreenBlue
//
//  Created by Ainor Syahrizal on 01/07/2016.
//  Copyright © 2016 Ainor Syahrizal. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showRed(sender: AnyObject) {
        
        //create instance of RedViewController
        let redVC: RedViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RedViewController") as! RedViewController
        
        self.presentViewController(redVC, animated: true, completion: nil)
    }
    
    
    @IBAction func dismiss(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        // because its in navigationController
        //self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
