//
//  RedViewController.swift
//  RedGreenBlue
//
//  Created by Ainor Syahrizal on 01/07/2016.
//  Copyright © 2016 Ainor Syahrizal. All rights reserved.
//

import UIKit

class RedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showGreen(sender: AnyObject) {
       
        // create instance
       let greenVC: GreenViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GreenViewController") as! GreenViewController
       
        
        let segue: UIStoryboardSegue = UIStoryboardSegue(identifier: "showGreenViewController", source: self, destination: greenVC) { 
            self.presentViewController(greenVC, animated: true, completion: nil)
        }
        
        segue.perform()
        // and add the instance into the screen
        //self.presentViewController(greenVC, animated: true, completion: nil)
        //self.navigationController?.pushViewController(greenVC, animated: true)
   }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
